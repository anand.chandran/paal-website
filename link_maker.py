"""
makes url query links from specified columns of table input
appends these links to a new column on the right, Requst Price
these RFQ links will lead to a contact form with this data pre-filled
"""
import sys
import urllib

my_link = ""
delimiter = ';'
spacer = "%0A"


first = True

#needed_cols = [1, 5, 6, 7]
needed_cols = [0]


file_loc = sys.argv[1];

infile = open(file_loc, 'r')
outfile = open("generated_links.csv", 'w')
titles = []

for line in infile:

	if first:
		outfile.write(line.strip() + delimiter + "Price Request\n")
		titles = line.strip().split(delimiter)
		first = False;
	else:
		my_link = "<a href=contact.html?msg="
		s = line.split(delimiter)

		for i in needed_cols:
			my_link += urllib.quote(titles[i] + ":\t")  + urllib.quote(s[i].strip()) + spacer
		my_link += ">RFQ</a>"
		outfile.write(line.strip() + delimiter + my_link + "\n")

infile.close()
outfile.close()
