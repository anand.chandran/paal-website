
"""
Makes an html table out of CSV, delimited text
assumes first row is table headers, does no error checking
"""

import sys

delimiter = ";"
file_loc = sys.argv[1]
infile = open(file_loc, 'r')
outfile = open("generated_table.html", 'w')

output = ""
outfile.write("<table>\n")

first = True
elem = ""

for line in infile:
	outfile.write("\t<tr>")
	if first:
		elem = "th"
		first = False
	else:
		elem = "td"
	values = line.split(delimiter)
	for v in values:
		outfile.write("\n\t\t<" + elem + ">")
		outfile.write(" " + v.strip())
		outfile.write(" </" + elem + ">")
	outfile.write("\n\t</tr>\n")

outfile.write("</table>\n")

outfile.close()
infile.close()
