<?php

ini_set('display_errors', 1); error_reporting(-1);

/* Set email recipient */
$myemail = "prem@paaltech.com";

/* Check all form inputs using check_input function */
$name = check_input($_POST['name'], "Enter your name");
$subject = "RFQ from website";
$email = check_input($_POST['email']);
$msg = check_input($_POST['msg'], "Write your message"); 
$headers = "From: paal_bot@paaltech.com";

/* If e-mail is not valid, show error message */
if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email))
{
	show_error("E-mail address not valid");
}

$formatted_msg = "
Name: $name
Email: $email
Subject: $subject

Message:
$msg

";
/* Send the messaged using mail() function */
mail($myemail, $subject, $formatted_msg, $headers);

/* Redirect visitor to the thank you page */
header('Location: thanks.html');
exit();


/* Functions we used */
function check_input($data, $problem='')
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	if ($problem && strlen($data) == 0)
	{
		show_error($problem);
	}
	return $data;
}

function show_error($myError)
{
?>
<html>
<body>

<p>Please correct the following error:</p>
<strong><?php echo $myError; ?></strong>
<p>Hit the back button and try again</p>

</body>
</html>
<?php
	exit();
}
?>
